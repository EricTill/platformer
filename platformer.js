//(function() { // module pattern

  //-------------------------------------------------------------------------
  // POLYFILLS
  //-------------------------------------------------------------------------
  
  if (!window.requestAnimationFrame) { // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    window.requestAnimationFrame = window.webkitRequestAnimationFrame || 
                                   window.mozRequestAnimationFrame    || 
                                   window.oRequestAnimationFrame      || 
                                   window.msRequestAnimationFrame     || 
                                   function(callback, element) {
                                     window.setTimeout(callback, 1000 / 60);
                                   };
  }

  //-------------------------------------------------------------------------
  // UTILITIES
  //-------------------------------------------------------------------------
  
  function timestamp() {
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
  }
  
  function bound(x, min, max) {
    return Math.max(min, Math.min(max, x));
  }

  function get(url, onsuccess) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if ((request.readyState == 4) && (request.status == 200))
        onsuccess(request);
    };
    request.open("GET", url, true);
    request.send();
  }

  function overlap(x1, y1, w1, h1, x2, y2, w2, h2) {
    return !(((x1 + w1 - 1) < x2) ||
             ((x2 + w2 - 1) < x1) ||
             ((y1 + h1 - 1) < y2) ||
             ((y2 + h2 - 1) < y1));
  }

  function maybeSetDefault(val, def) {
    if (val === undefined)
      return def;
    else
      return val;
  }

  //Return a real min to max (inclusive)
  function getUnif(a, b) {
      return Math.random() * (b - a) + a;
  }
  
  //Return an integer min to max (inclusive)
  function getRandInt(a,b) {
      return Math.round(getUnif(a,b));
  }
  
  //Constrain angle to [0,2*pi)
  function circConstrain (theta) {
      return theta-2*Math.PI*Math.floor(theta/(2*Math.PI));
  }

  function STOP () {RUNNING = !RUNNING;};

  
  //-------------------------------------------------------------------------
  // GAME CONSTANTS AND VARIABLES
  //-------------------------------------------------------------------------
  
  var SCREEN   = { tw: 64, th: 48 },
      LEVEL    = { w: 512, h: 48},
      TILE     = 32,       //(Measured in canvas context pixels!)
      METER    = TILE,
      GRAVITY  = 9.8 * 10, // default (exagerated) gravity
      MAXDX    = 15,      // default max horizontal speed (15 tiles per second)
      MAXDY    = 60,      // default max vertical speed   (60 tiles per second)
      ACCEL    = 1/10,     // default take 1/2 second to reach maxdx (horizontal acceleration)
      FRICTION = 1/8,     // default take 1/6 second to stop from maxdx (horizontal friction)
      IMPULSE  = 2000,    // default player jump impulse
      COLOR    = { BLACK: '#000000', YELLOW: '#ECD078', BRICK: '#D95B43', PINK: '#C02942', PURPLE: '#542437', GREY: '#333', SLATE: '#53777A', GOLD: 'gold' },
      COLORS   = [ COLOR.YELLOW, COLOR.BRICK, COLOR.PINK, COLOR.PURPLE, COLOR.GREY ],
      KEY      = { SPACE: 32, LEFT: 37, UP: 38, RIGHT: 39, DOWN: 40 },
      RUNNING  = true;
      
  var fps      = 60,
      step     = 1/fps,
      canvas   = document.getElementById('canvas'),
      ctx      = canvas.getContext('2d'),
      width    = canvas.width  = SCREEN.tw * TILE,
      height   = canvas.height = SCREEN.th * TILE,
      player   = {},
      monsters = [],
      treasure = [],
      bullets  = [],
      level    = [],
      offset   = 0;
  
  var t2p      = function(t)     { return t*TILE;                     },
      p2t      = function(p)     { return Math.floor(p/TILE);         },
      p2unit   = function(p)     { return p/TILE;                     },
      tcell    = function(tx,ty) { return level[tx + LEVEL.w*ty];     }; 
  
  //-------------------------------------------------------------------------
  // SPAWNING FUNCTIONS
  //-------------------------------------------------------------------------
  
  function spawnBullet(player) {
    var left = false,
        right = false;
    switch(player.facing) {
      case -1: left = true; break;
      case 1: right = true; break;
      default: right = true;
    }
    var obj = {
        x: player.x,
        y: player.y,
        dx: player.facing * 30,
        dy: getUnif(-50,50),
        properties: {
          gravity: 0,
          maxdx: 50,
          maxdy: 50,
          accel: 0,
          friction: 0,
          left: left,
          right: right
        },
        type: "bullet"
    };
    bullets.push(setupEntity(obj));
  }


  //-------------------------------------------------------------------------
  // UPDATE LOOP
  //-------------------------------------------------------------------------

  function onkey(ev, key, down) {
    switch(key) {
      case KEY.LEFT:  player.left  = down; ev.preventDefault(); return false;
      case KEY.RIGHT: player.right = down; ev.preventDefault(); return false;
      case KEY.SPACE: player.shoot = down; ev.preventDefault(); return false;
      case KEY.UP:    player.jump  = down; ev.preventDefault(); return false;
      default: return false;
    }
  }
  
  function update(dt) {
    updatePlayer(dt);
    updateMonsters(dt);
    updateBullets(dt);
    checkTreasure();
  }

  function updatePlayer(dt) {
    player.facing = player.left ? -1 : player.right ? 1 : player.facing;
    updateEntity(player, dt);
    if (player.shoot) {
      spawnBullet(player);
    }
  }

  function updateMonsters(dt) {
    var n, max;
    for(n = 0, max = monsters.length ; n < max ; n++)
      updateMonster(monsters[n], dt);
  }

  function updateBullets(dt) {
    var n, max;
    for(n = 0, max = bullets.length ; n < max ; n++)
      updateBullet(bullets[n], dt);
  }

  function updateMonster(monster, dt) {
    if (!monster.dead) {
      updateEntity(monster, dt);
      if (overlap(player.x, player.y, TILE, TILE, monster.x, monster.y, TILE, TILE)) {
        if ((player.dy > 0) && (monster.y - player.y > TILE/2))
          killMonster(monster);
        else
          killPlayer(player);
      }
    }
  }

  function updateBullet(bullet, dt) {
    if (!bullet.dead) {
      updateEntity(bullet, dt);
      var n, max;
      for (n = 0, max = monsters.length; n < max; n++) {
        var monster = monsters[n];
        if (!monster.dead) {
            if (overlap(bullet.x, bullet.y, TILE, TILE, monster.x, monster.y, TILE, TILE)) {
              killMonster(monster);
              killBullet(bullet);
            }
        }
      }
    }
  }

  function checkTreasure() {
    var n, max, t;
    for(n = 0, max = treasure.length ; n < max ; n++) {
      t = treasure[n];
      if (!t.collected && overlap(player.x, player.y, TILE, TILE, t.x, t.y, TILE, TILE))
        collectTreasure(t);
    }
  }

  function checkXDistFromPlayer(entity) {
    var dist = Math.abs(entity.x - player.x);
    if (dist > (SCREEN.tw/2)*TILE)
      return false;
    else
      return true;
  }

  function translatePosToScreen(entity) {
    //Should translate global x coords to screen-relative values
    return (entity.x - player.x) + SCREEN.tw/2*TILE;
  }

  function killMonster(monster) {
    player.killed++;
    monster.dead = true;
  }

  function killPlayer(player) {
    player.x = player.start.x;
    player.y = player.start.y;
    player.dx = player.dy = 0;
  }

  function killBullet(bullet) {
    bullet.dead = true;
  }

  function collectTreasure(t) {
    player.collected++;
    t.collected = true;
  }

  function updateEntity(entity, dt) {
    var wasleft    = entity.dx  < 0,
        wasright   = entity.dx  > 0,
        falling    = entity.falling,
        friction   = entity.friction * (falling ? 0.5 : 1),
        accel      = entity.accel    * (falling ? 0.5 : 1);
  
    entity.ddx = 0;
    entity.ddy = entity.gravity;
  
    if (entity.left)
      entity.ddx = entity.ddx - accel;
    else if (wasleft)
      entity.ddx = entity.ddx + friction;
  
    if (entity.right)
      entity.ddx = entity.ddx + accel;
    else if (wasright)
      entity.ddx = entity.ddx - friction;
  
    if (entity.jump && !entity.jumping && !falling) {
      entity.ddy = entity.ddy - entity.impulse; // an instant big force impulse
      entity.jumping = true;
    }

    entity.x  = entity.x  + (dt * entity.dx);
    entity.y  = entity.y  + (dt * entity.dy);
    entity.dx = bound(entity.dx + (dt * entity.ddx), -entity.maxdx, entity.maxdx);
    entity.dy = bound(entity.dy + (dt * entity.ddy), -entity.maxdy, entity.maxdy);
  
    if ((wasleft  && (entity.dx > 0)) ||
        (wasright && (entity.dx < 0))) {
      entity.dx = 0; // clamp at zero to prevent friction from making us jiggle side to side
    }
  
    var tx        = p2t(entity.x),
        ty        = p2t(entity.y),
        nx        = entity.x%TILE,
        ny        = entity.y%TILE,
        cell      = tcell(tx,     ty),
        cellright = tcell(tx + 1, ty),
        celldown  = tcell(tx,     ty + 1),
        celldiag  = tcell(tx + 1, ty + 1);
  
    //Collision with floor/ceiling
    if (entity.dy > 0) {
      if ((celldown && !cell) ||
          (celldiag && !cellright && nx)) {
        entity.y = t2p(ty);
        entity.dy = 0;
        entity.falling = false;
        entity.jumping = false;
        ny = 0;
        if (entity.bullet)
          killBullet(entity);
      }
    }
    else if (entity.dy < 0) {
      if ((cell      && !celldown) ||
          (cellright && !celldiag && nx)) {
        entity.y = t2p(ty + 1);
        entity.dy = 0;
        cell      = celldown;
        cellright = celldiag;
        ny        = 0;
        if (entity.bullet)
          killBullet(entity);
      }
    }
  
    //Collisions with walls
    if (entity.dx > 0) {
      if ((cellright && !cell) ||
          (celldiag  && !celldown && ny)) {
        entity.x = t2p(tx);
        entity.dx = 0;
        if (entity.bullet)
          killBullet(entity);
      }
    }
    else if (entity.dx < 0) {
      if ((cell     && !cellright) ||
          (celldown && !celldiag && ny)) {
        entity.x = t2p(tx + 1);
        entity.dx = 0;
        if (entity.bullet)
          killBullet(entity);
      }
    }

    if (entity.monster) {
      if (entity.left && (cell || !celldown)) {
        entity.left = false;
        entity.right = true;
      }      
      else if (entity.right && (cellright || !celldiag)) {
        entity.right = false;
        entity.left  = true;
      }
    }
  
    entity.falling = ! (celldown || (nx && celldiag));
  
  }

  //-------------------------------------------------------------------------
  // RENDERING
  //-------------------------------------------------------------------------
  
  function render(ctx, frame, dt) {
    ctx.clearRect(0, 0, width, height);
    renderMap(ctx);
    renderTreasure(ctx, frame);
    renderPlayer(ctx, dt);
    renderMonsters(ctx, dt);
    renderBullets(ctx, dt);
  }

  function renderMap(ctx) {
    var x, y, cell, offset;
    offset = player.x % TILE;
    for(y = 0 ; y < SCREEN.th ; y++) {
      for(x = -SCREEN.tw/2-1; x < SCREEN.tw/2+1 ; x++) {
        cell = tcell(x + p2t(player.x), y);
        if (cell > 0) {
          ctx.fillStyle = COLORS[cell - 1];
          ctx.fillRect((x+SCREEN.tw/2)*TILE - Math.round(offset), y * TILE, TILE, TILE);
        }
      }
    }
  }

  function renderPlayer(ctx, dt) {
    ctx.fillStyle = COLOR.YELLOW;
    ctx.fillRect(translatePosToScreen(player) + (player.dx * dt), player.y + (player.dy * dt), TILE, TILE);

    var n, max;

    ctx.fillStyle = COLOR.GOLD;
    for(n = 0, max = player.collected ; n < max ; n++)
      ctx.fillRect(t2p(2 + n), t2p(2), TILE/2, TILE/2);

    ctx.fillStyle = COLOR.SLATE;
    for(n = 0, max = player.killed ; n < max ; n++)
      ctx.fillRect(t2p(2 + n), t2p(3), TILE/2, TILE/2);
  }

  function renderBullets(ctx, dt) {
    ctx.fillStyle = COLOR.YELLOW;
    var n, max, bullet;
    for(n = 0, max = bullets.length ; n < max ; n++) {
      bullet = bullets[n];
      if (!bullet.dead && checkXDistFromPlayer(bullet))
        ctx.fillRect(translatePosToScreen(bullet) + (bullet.dx * dt), bullet.y + (bullet.dy * dt), TILE, TILE/6);
    }
  }

  function renderMonsters(ctx, dt) {
    ctx.fillStyle = COLOR.SLATE;
    var n, max, monster;
    for(n = 0, max = monsters.length ; n < max ; n++) {
      monster = monsters[n];
      if (!monster.dead && checkXDistFromPlayer(monster))
        ctx.fillRect(translatePosToScreen(monster) + (monster.dx * dt), monster.y + (monster.dy * dt), TILE, TILE);
    }
  }

  function renderTreasure(ctx, frame) {
    ctx.fillStyle   = COLOR.GOLD;
    ctx.globalAlpha = 0.25 + tweenTreasure(frame, 60);
    var n, max, t;
    for(n = 0, max = treasure.length ; n < max ; n++) {
      t = treasure[n];
      if (!t.collected && checkXDistFromPlayer(t))
        ctx.fillRect(translatePosToScreen(t), t.y + TILE/3, TILE, TILE*2/3);
    }
    ctx.globalAlpha = 1;
  }

  function tweenTreasure(frame, duration) {
    var half  = duration/2,
        pulse = frame%duration;
    return pulse < half ? (pulse/half) : 1-(pulse-half)/half;
  }

  //-------------------------------------------------------------------------
  // LOAD THE MAP
  //-------------------------------------------------------------------------
  
  function setup(map) {
    var data    = map.layers[0].data,
        objects = map.layers[1].objects,
        n, obj, entity;

    for(n = 0 ; n < objects.length ; n++) {
      obj = objects[n];
      entity = setupEntity(obj);
      switch(obj.type) {
      case "player"   : player = entity; break;
      case "monster"  : monsters.push(entity); break;
      case "treasure" : treasure.push(entity); break;
      }
    }

    level = data;
  }

  function setupEntity(obj) {
    var entity = {};
    entity.x        = obj.x;
    entity.y        = obj.y;
    entity.dx       = maybeSetDefault(obj.dx,0);
    entity.dy       = maybeSetDefault(obj.dy,0);
    entity.gravity  = METER * maybeSetDefault(obj.properties.gravity, GRAVITY);
    entity.maxdx    = METER * maybeSetDefault(obj.properties.maxdx  , MAXDX);
    entity.maxdy    = METER * maybeSetDefault(obj.properties.maxdy  , MAXDY);
    entity.impulse  = METER * maybeSetDefault(obj.properties.impulse, IMPULSE);
    entity.accel    = entity.maxdx / maybeSetDefault(obj.properties.accel    , ACCEL);
    entity.friction = entity.maxdx / maybeSetDefault(obj.properties.friction , FRICTION);
    entity.monster  = obj.type == "monster";
    entity.player   = obj.type == "player";
    entity.treasure = obj.type == "treasure";
    entity.bullet   = obj.type == "bullet";
    entity.left     = obj.properties.left;
    entity.right    = obj.properties.right;
    entity.start    = { x: obj.x, y: obj.y };
    entity.facing   = obj.properties.facing;
    entity.killed = entity.collected = 0;
    return entity;
  }

  //-------------------------------------------------------------------------
  // THE GAME LOOP
  //-------------------------------------------------------------------------
  
  var counter = 0, dt = 0, now,
      last = timestamp(),
      fpsmeter = new FPSMeter({ decimals: 0, graph: true, theme: 'dark', left: '5px' });
  
  function frame() {
    fpsmeter.tickStart();
    now = timestamp();
    dt = dt + Math.min(1, (now - last) / 1000);
    while(dt > step) {
      dt = dt - step;
      update(step);
    }
    render(ctx, counter, dt);
    last = now;
    counter++;
    fpsmeter.tick();
    if (RUNNING)
      requestAnimationFrame(frame, canvas);
  }
  
  document.addEventListener('keydown', function(ev) { return onkey(ev, ev.keyCode, true);  }, false);
  document.addEventListener('keyup',   function(ev) { return onkey(ev, ev.keyCode, false); }, false);

  //get("level.json", function(req) {
  get("wide.json", function(req) {
    setup(JSON.parse(req.responseText));
    frame();
  });

//})();

